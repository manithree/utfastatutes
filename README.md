# Utah {Generic} Statutes Document Generator

There are lots of statutes at le.utah.gov that pertain to firearms: ownership,
selling, buying, carrying, defensive use, etc. Sometimes you might (I do,
anyway) want to have just the firearms-related statutes available offline. This
program aims to accomplish that.

And in it's latest version, it is now a more generic html booklet maker for
subsets of Utah code. Or, a specialized le.utah.gov scraper.

This latest version simplifies the settings file format, and includes one for
most bicycle-related statutes.

## Current

This is a standalone nim program that's primary purpose is to download the
latest version of statute sections (chapter, parts, sections, or subsections)
that relate to firearms, and optionally transform them.

It is configured with the settings `.json` file. Allowed values for the
`download_format` are:

    xml
    rtf
    pdf

The only supported `output_format` is `html`. The `output_format` will be
ignored unless the `download_format` is `xml`.

When `download_format` is `xml` and `output_format`is `html` a single html page
will be generated containing all the statues, and a linked table of contents.
The xml files and the generated html file are saved in the cwd where the program
is run.

Two new required settings are `title` for the html title element and initial
H1, and `filename` for the destination html file.

## Building

To build from source:

    git clone https://gitlab.com/manithree/utfastatutes.git
    cd utfastatutes
    nimble build
  
Then to run it, just:

    ./utfastatutes_nim settings.json


## History

I originally wrote a groovy script that downloaded the .rtf files from
le.utah.gov and used libreoffice and OoOPy to put it all together in one
document. I discovered later that I hadn't properly reverse engineered
le.utah.gov and it didn't always download the latest versions. It also didn't
have a table of contents.

So, I re-wrote it in rust, and fixed it so it that it always got the latest
version of the statute. I couldn't find a good ODF library in rust, but there's
a really good Java one, so I wrote another groovy script to take xml files
downloaded by the rust program and generate a libreoffice document with a table
of contents.

The next step was writing a groovy script to convert the xml to very simple
html with a table of contents. Then you don't need google doc/drive to view the
document, and you don't need any libreoffice tools at document generation time.

All that happened over the last several years on github.

This is a re-write in Nim (yeah, this project is my excuse to learn a new
language occasionally), and combine the download and html generation into one
package. I strongly prefer the html output, so I didn't include the ODF output
in the nim re-write. The latest version will download any format you configure
it for, but will only generate HTML (and only from XML).
