# Package

version       = "0.1.0"
author        = "Barry Roberts"
description   = "Read Utah firearm statutes (xml) from le.utah.gov and create an html document"
license       = "MIT"
srcDir        = "src"
bin           = @["utfastatutes_nim"]


# Dependencies

requires "nim >= 0.19"

when defined(nimdistros):
  import distros
  if detectOs(Ubuntu):
    foreignDep "libssl-dev"
  else:
    foreignDep "openssl"
    
#foreignDep "openssl"
